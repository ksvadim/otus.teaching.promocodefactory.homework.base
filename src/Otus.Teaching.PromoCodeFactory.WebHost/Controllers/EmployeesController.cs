﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Контроллер сотрудников.
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получает данные всех сотрудников.
        /// </summary>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получает данные сотрудника по Id.
        /// </summary>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создает нового сотрудника.
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateAsync(EmployeeModel employeeModel)
        {
           var allRoles = await _roleRepository.GetAllAsync();

           var roles = allRoles
              .Where(x => employeeModel.RoleIds.Contains(x.Id))
              .ToList();

           if (roles.Count != employeeModel.RoleIds.Count)
              return BadRequest("Role not found");

           var employee = new Employee
           {
               FirstName = employeeModel.FirstName,
               LastName = employeeModel.LastName,
               Email = employeeModel.Email,
               Roles = roles
           };

           return await _employeeRepository.CreateAsync(employee);
        }

        /// <summary>
        /// Обновляет сотрудника.
        /// </summary>
        [HttpPut]
        public async Task<ActionResult<EmployeeResponse>> UpdateAsync(EmployeeModel employeeModel)
        {
           if (employeeModel.Id == null)
              return BadRequest();

           var employeeToUpdate = await _employeeRepository.GetByIdAsync(employeeModel.Id.Value);

           if (employeeToUpdate == null)
              return NotFound();

           employeeToUpdate.FirstName = employeeModel.FirstName;
           employeeToUpdate.LastName = employeeModel.LastName;
           employeeToUpdate.Email = employeeModel.Email;

           if (employeeModel.AppliedPromocodesCount != null)
              employeeToUpdate.AppliedPromocodesCount = employeeModel.AppliedPromocodesCount.Value;

           var allRoles = await _roleRepository.GetAllAsync();

           var roles = allRoles
              .Where(x => employeeModel.RoleIds.Contains(x.Id))
              .ToList();

           if (roles.Count != employeeModel.RoleIds.Count)
              return BadRequest("Role not found");

           employeeToUpdate.Roles = roles;
           
           var employee = await _employeeRepository.UpdateAsync(employeeToUpdate);

           return new EmployeeResponse
           {
              Id = employee.Id,
              Email = employee.Email,
              Roles = employee.Roles
                 .Select(x =>
                    new RoleItemResponse
                    {
                       Name = x.Name,
                       Description = x.Description
                    })
                 .ToList(),
              FullName = employee.FullName,
              AppliedPromocodesCount = employee.AppliedPromocodesCount
           };
        }

        /// <summary>
        /// Удаляет сотрудника.
        /// </summary>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteAsync(Guid employeeId)
        {
           var employeeToDelete = await _employeeRepository.GetByIdAsync(employeeId);

           if (employeeToDelete == null)
              return NotFound();

           await _employeeRepository.DeleteAsync(employeeToDelete);
           return Ok();
        }
    }
}